<?php


namespace Centralpos\ServicesClient;


use Exception;
use GuzzleHttp\Client;

class ServicesClientManager
{
    /**
     * @var array
     */
    protected $connections = [];

    /**
     * @param  string|null $connection
     * @return Client
     * @throws Exception
     */
    public function connection(string $connection = null)
    {
        $connection = $connection ?: $this->defaultConnectionName();

        if (!isset($this->connections[$connection])) {
            $this->connections[$connection] = $this->makeConnection($connection);
        }

        return $this->connections[$connection];
    }

    /**
     * @param  string $connection
     * @return Client
     * @throws Exception
     */
    protected function makeConnection(string $connection)
    {
        $config = $this->connectionConfig($connection);

        if (!$config) {
            throw new Exception("Connection [$connection] config not found");
        }

        return app(Client::class, compact('config'));
    }

    /**
     * @return string
     */
    protected function defaultConnectionName()
    {
        return config("services.default");
    }

    /**
     * @param  string $connection
     * @return array|null
     */
    protected function connectionConfig(string $connection)
    {
        return config("services.connections.$connection");
    }

    /**
     * @param  string $method
     * @param  array $arguments
     * @return mixed
     * @throws Exception
     */
    public static function __callStatic(string $method, array $arguments)
    {
        return call_user_func_array([(new static())->connection(), $method], $arguments);
    }

    /**
     * @param  string $method
     * @param  array $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call(string $method, array $arguments)
    {
        return call_user_func_array([$this->connection(), $method], $arguments);
    }
}
