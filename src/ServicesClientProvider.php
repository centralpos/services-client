<?php

namespace Centralpos\ServicesClient;

use Illuminate\Support\ServiceProvider;

class ServicesClientProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/services.php'),'services');

        $this->app->singleton('ServicesClient', function ($app) {
            return new ServicesClientManager();
        });
    }
}