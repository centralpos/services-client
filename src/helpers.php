<?php

if (! function_exists('services')) {
    /**
     * Get the configuration path.
     *
     * @param  null|string $connection
     * @return GuzzleHttp\Client
     */
    function services($connection = null)
    {
        return app('ServicesClient')
            ->connection($connection);
    }
}
