<?php
return [
    'default' => env('SERVICE_CLIENT_CONNECTION', 'default'),

    'connections' => [

        'default' => [
            'base_uri' => env('SERVICE_CLIENT_URI'),
            'headers' => [
                'apikey' => env('SERVICE_CLIENT_APIKEY')
            ],
        ],
    ],
];
